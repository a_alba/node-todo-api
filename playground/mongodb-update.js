// ES6 Object destructruing
const {MongoClient, ObjectID} = require('mongodb');

MongoClient.connect('mongodb://localhost:27017/TodoApp', (err, client) => {
    if(err) {
        return console.log('Unable to connect to MongoDB server');
    }
    console.log('Connected to MongoDB server');
    const db = client.db('TodoApp');
    
    /*db.collection('Todos').findOneAndUpdate({
        _id: new ObjectID('5c310eb34b986b9df515d320')
    }, {
        $set: {
            completed: true
        }
    }, {
        returnOriginal: false
    }).then((result) => {
        console.log(result);
    });*/

    db.collection('Users').findOneAndUpdate({
        _id: new ObjectID('5c290111d52be0067c7d65de')
    }, {
        $set: {
            name: 'Andrew'
        },
        $inc: {
            age: 1
        } 
    }, {
        returnOriginal: false
    }).then((result) => {
        console.log(result);
    });

    //client.close();
});