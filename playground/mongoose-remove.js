const {ObjectID} = require('mongodb');

const {mongoose} = require('./../server/db/mongoose');
const {Todo} = require('./../server/models/todo');
const {User} = require('./../server/models/user');

// Removes all todos
/*Todo.remove({}).then((result) => {
    console.log(result);
});*/

Todo.findOneAndRemove({
    _id: '5c4490242f62e841695a0a28'
}).then((todo) => {
    console.log(todo);
})

Todo.findByIdAndRemove('5c4490242f62e841695a0a28').then((todo) => {
    console.log(todo);
});