# Section 7: MongoDB, Mongoose and REST APIs
## NoSQL vocabulary
* A server can have multiple databases
* Each database, contains Collections which are the same as SQL Tables
* Rows/records in Mongo are called Documents and Columns are Fields

## Deploying API in Heroku
1. Run `heroku create`
2. Install *mongolab* add-on
```sh
herouke addons:create mongolab:sandbox
```
3. Push application to heroku remote
```sh
git push heroku master
```

# Section 8: Security and authentication
## Configuring ENV variables in Heroku
* Set a new enviroment variable:
```sh
heroku config:set {ENV_VAR_NAME}={env_var_value}
```

* Get enviroment variables:
```sh
heroku config
```

* Get a given environment variable value
```sh
heroku config:get {ENV_VAR_NAME}
```

* Unset an enviroment variable:
```sh
heroku config:unset {ENV_VAR_NAME}
```

## Advanced POSTMAN
### Set enviroment variables and use them for later requests
* Write the following script in the tests sections, which will fetch the x-auth token and save it
```js
var token = postman.getResponseHeader('x-auth');
postman.setEnvironmentVariable('x-auth', token);
```
* Use the variable with the syntax `{{variable_name}}`

